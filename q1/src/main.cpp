#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <stdlib.h>

#define SSID "inc2"
#define SSID_PASSWOD "12345678"

WiFiUDP Udp;

#ifdef DEBUG
	#define PRINT_DBG(...) printf
#else
	#define PRINT_DBG(...) 
#endif

String a;
int period1 = 1000, period2= 1000;
char response[255];
char packetBuffer[255]; // buffer to hold incoming packet

void loop_udp();
void loop_print();
void loop_LED();

void setup()
{
	srand(0);
	Serial.begin(115200);
	pinMode(LED_BUILTIN, OUTPUT);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());

	Udp.begin(9999);
}
void loop()
{
	loop_udp();
	loop_print();
	loop_LED();
}

void loop_print()
{
	static unsigned long previous_time = 0;
	if(millis() - previous_time > period1)
	{
		previous_time = millis();
		Serial.println("Hello!");
	}
}

void loop_LED()
{
	static unsigned long previous_time = 0;
	static uint8_t flag = 0;
	if(millis() - previous_time > period2)
	{
		previous_time = millis();
		digitalWrite(LED_BUILTIN, flag);
		flag = flag == 1 ? 0 : 1;
	}
}

void loop_udp()
{
	StaticJsonDocument<200> docResp;
	StaticJsonDocument<200> docReq;

	int packetSize = Udp.parsePacket();

	if (packetSize)
	{

		Serial.print("Received packet of size ");
		Serial.println(packetSize);
		Serial.print("From ");
		IPAddress remoteIp = Udp.remoteIP();
		Serial.print(remoteIp);
		Serial.print(", port ");
		Serial.println(Udp.remotePort());

		// read the packet into packetBufffer

		int len = Udp.read(packetBuffer, 255);

		if (len > 0)
		{
			packetBuffer[len] = 0;
		}

		Serial.println("Contents:");
		Serial.println(packetBuffer);

		// Deserialize the JSON document
		DeserializationError error = deserializeJson(docReq, packetBuffer);

		// Test if parsing succeeds.
		if (error)
		{
			Serial.print(F("deserializeJson() failed: "));
			Serial.println(error.f_str());
			return;
		}

		// send a reply, to the IP address and port that sent us the packet we received

		const char *str1 = docReq["T1"];
		const char *str2 = docReq["T2"];

		if (docReq.containsKey("T1") && docReq.containsKey("T2"))
		{
			period1 = atoi(str1);
			period2 = atoi(str2);
		}
		else
		{
			Serial.println("Error");
		}

		// serializeJson(docResp, response, sizeof(response));
		// Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
		// Udp.write((uint8_t *)response, strlen(response));
		// Udp.endPacket();
	}
}