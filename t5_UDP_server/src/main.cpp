#include <Arduino.h>
#include <WiFi.h>

#define SSID "www"
#define SSID_PASSWOD "m33553502"

String a;

WiFiUDP Udp;

char packetBuffer[255];              // buffer to hold incoming packet
char ReplyBuffer[] = "acknowledged"; // a string to send back

void setup()
{
  Serial.begin(115200);
  // Connect to WiFi
  WiFi.begin(SSID, SSID_PASSWOD);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
  // Print the IP address
  Serial.println(WiFi.localIP());
  Udp.begin(30000);
  
}

void loop()
{
  // if there's data available, read a packet

  int packetSize = Udp.parsePacket();

  if (packetSize)
  {

    Serial.print("Received packet of size ");

    Serial.println(packetSize);

    Serial.print("From ");

    IPAddress remoteIp = Udp.remoteIP();

    Serial.print(remoteIp);

    Serial.print(", port ");

    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer

    int len = Udp.read(packetBuffer, 255);

    if (len > 0)
    {

      packetBuffer[len] = 0;
    }

    Serial.println("Contents:");

    Serial.println(packetBuffer);
  

    // send a reply, to the IP address and port that sent us the packet we received

    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());

    Udp.write((uint8_t *)ReplyBuffer, sizeof(ReplyBuffer));

    Udp.endPacket();
  }
}