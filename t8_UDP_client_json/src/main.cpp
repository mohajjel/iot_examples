#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <stdlib.h>

#define SSID "www"
#define SSID_PASSWOD "m33553502"

#define SERVER_IP "192.168.100.3"
#define SERVER_PORT 30000

WiFiUDP Udp;

String a;
char response[255];
char packetBuffer[255]; // buffer to hold incoming packet

void setup()
{
	srand(0);
	Serial.begin(115200);
	// Connect to WiFi
	WiFi.begin(SSID, SSID_PASSWOD);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());

	Udp.begin(9999);
}

void loop()
{
	StaticJsonDocument<200> docResp;
	StaticJsonDocument<200> docReq;

	static long previous_time = 0;
	if (millis() - previous_time > 5000)
	{
		previous_time = millis();
		docResp["sensor"] = "gps";
		docResp["time"] = millis();

		JsonArray data = docResp.createNestedArray("data");
		data.add(rand() % 100);
		data.add(rand() % 100);

		serializeJson(docResp, response, sizeof(response));
		Udp.beginPacket(SERVER_IP, SERVER_PORT);
		Udp.write((uint8_t *)response, strlen(response));
		Udp.endPacket();

		//int packetSize = Udp.parsePacket();
	}
}