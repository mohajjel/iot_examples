#include <Arduino.h>

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.println("Hello high!");
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.println("Hello! low");
  delay(1000);
}