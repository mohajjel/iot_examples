#include <Arduino.h>
String a;

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
}

void loop()
{
  if(Serial.available()>0)
  {
    a = Serial.readStringUntil('t'); // read the incoming data as string
    Serial.println(a);
    if(a[0]=='1')
      digitalWrite(LED_BUILTIN, HIGH);
    else
      digitalWrite(LED_BUILTIN, LOW);
  }
}